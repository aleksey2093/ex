contract KingOfTheEtherThroneMod {
    address payable public king;
    uint public claimPrice = 100;
    address owner;

    constructor() public {
        owner = msg.sender;
        king = msg.sender;
    }

    function() external payable {
        require(msg.value >= claimPrice,'Недостаточно средств');
        uint compensation = calculateCompenstation();
        (bool success, ) = address(king).call.value(compensation)('');
        king = msg.sender;
        calculateNewPrice();
    }

    function calculateCompenstation() public returns (uint) {
        ...
    }

    function calculateNewPrice() public {
        ...
    }
}