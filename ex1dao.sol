
contract DAOEx1 {
    mapping (address => unit) credit;

    function donate(address to) public payable {
        credit[to] = credit[to] + msg.value;
    }

    function wthdraw(unit n) public payable {
        if (credit[msg.sender] >= n) {
            msg.sender.call.value(n)('');
            credit[msg.sender] -= n;
        }
    }

    function queryCredit() public view returns (unit) {
        return credit[msg.sender]
    }
}
