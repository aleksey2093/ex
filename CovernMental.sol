function invest() public payable {
    require(msg.value >= (jackpot / 2));
    jackpot = jackpot + (msg.value / 2);
    lastInvestor = msg.sender;
    lastInvestTs = block.timestamp;
}

function resetInvestment() public {
    require(block.timestamp >= (lastInvestTs + 60 * 1000))
    lastInvestor.send(jackpot);
    owner.send(address(this).balance - 1 ether);
    lastInvestor = address(uint160(0));
    jackpot = 1 ether;
    lastInvestTs = 0;
}